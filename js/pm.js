var animation = bodymovin.loadAnimation({
  // animationData: { /* ... */ },
  container: document.getElementById('container'), // required
  path: '../Flow 2.json', // required
  renderer: 'svg', // required
  loop: true, // optional
  autoplay: true, // optional
  name: "Demo Animation", // optional
});
$(document).ready(function () {
  $("#carousel").owlCarousel({
    items: 6,
    margin: 35,
    dot: false,
    nav: true,
    loop: true,
    center: true,
    autoplay: true,
    autoplayTimeout: 2000,
    autoplayHoverPause: false,
    responsiveClass: true,
    // animateOut: 'scale',
    // animateIn: 'fadeIn',
    responsive: {
      0: {
        items: 1
      },
  
      600: {
        items: 3
      },
  
      1024: {
        items: 6
      },
  
  
    }
  });


  $("#productSlider").owlCarousel({
    // autoplay: true,
    // rewind: true, /* use rewind if you don't want loop */
    // margin: 35,
    //  /*
    // animateOut: 'fadeOut',
    // animateIn: 'fadeIn',
    // */
    // autoHeight: true,
    // autoplayTimeout: 7000,
    // smartSpeed: 800,
    // nav: true,
    // responsiveClass: true,

    // responsive: {
    //   0: {
    //     items: 1
    //   },
  
    //   600: {
    //     items: 3
    //   },
  
    //   1024: {
    //     items: 4
    //   },
  
    //   1366: {
    //     items: 4
    //   }
    // }
    margin: 30,
    dot: false,
    nav: true,
    center: true,
    loop: true,
    autoplay: true,
    autoplayTimeout: 2000,
    autoplayHoverPause: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            // nav:true
        },
        600:{
            items:3,
            // nav:false
        },
        1024:{
            items:6,
            // nav:true,
            // loop:false
        }
    }
  });

});


function changecolor() {
  let usd = document.getElementById("usd");
  usd.style.color = "#B5B7C4";
  usd.style.borderBottom = "none";
  let egp = document.getElementById("egp");
  egp.style.color = "black";
  egp.style.borderBottom = "1px solid black";
}
function changecolor2() {
  let egp = document.getElementById("egp");
  egp.style.color = "#B5B7C4";
  egp.style.borderBottom = "none";
  let usd = document.getElementById("usd");
  usd.style.color = "black";
  usd.style.borderBottom = "1px solid black";
}
function changecolor3() {
  let mon = document.getElementById("mon");
  mon.style.color = "#B5B7C4";
  let year = document.getElementById("year");
  year.style.color = "black";
}
